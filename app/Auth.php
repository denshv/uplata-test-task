<?php

namespace app;

/**
 * Auth is a class that simple implements authentication in forum
 */
class Auth
{

    /**
     * Params of login form
     *
     * @var array
     */
    protected $params = [];

    /**
     * Instance of Request
     *
     * @var Request
     */
    protected $request = null;

    /**
     * Url of login form
     *
     * @var string
     */
    protected $url = null;
    

    /**
     * Function construct - prepare params for authentification
     *
     * @param string $url
     * @param string $login
     * @param string $pass
     */
    public function __construct(string $url, string $login, string $pass)
    {
        $this->params = [
            'do' => 'login',
            'vb_login_md5password' => md5($pass),
            'vb_login_md5password_utf' => md5($pass),
            's' => '',
            'securitytoken' => 'guest',
            'vb_login_username' => $login,
            'vb_login_password' => '',
            'cookieuser' => 1,
        ];

        $this->url = $url;

        $this->request = new Request();
    }

    /**
     * Function process authentication request
     *
     * @param string $url
     * @param string $login
     * @param string $pass
     */
    public function login()
    {
        return $this->request->post($this->url, $this->params);
    }

    public function logout()
    {


    }
}