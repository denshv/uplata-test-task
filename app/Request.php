<?php

namespace app;

/**
 * Request is a class that simple implements HTTP Request
 */
class Request
{

    /**
     * Url
     *
     * @var string
     */
    protected $url = null;

    /**
     * Type of Http Request
     *
     * @var string
     */
    protected $type = 'GET';

    /**
     * Params of Http Request
     *
     * @var array
     */
    protected $params = [];

    public function __construct()
    {

    }

    /**
     * Proceess GET request
     *
     * @param string $url
     * @param array $params
     * 
     * @return mixed
     */
    public function get(string $url, array $params = [])
    {
        $this->url = $url;
        $this->params = $params;

        return $this->process();
    }

    /**
     * Proceess POST request
     *
     * @param string $url
     * @param array $params
     * 
     * @return mixed
     */
    public function post(string $url, array $params = [])
    {

        $this->url = $url;
        $this->type = 'POST';
        $this->params = $params;

        return $this->process();
    }


    /**
     * Proceess request
     *
     * @return mixed
     */
    protected function process()
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_HEADER, 1);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $header = array();
        $header[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8';
        $header[] = 'Accept-Charset: Windows-1251,utf-8;q=0.7,*;q=0.7';
        $header[] = 'Accept-Language: ru-ru,ru;q=0.8,en-us;q=0.5,en;q=0.3';
        $header[] = 'Pragma: ';
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);

        if ($this->type == 'POST') {
            curl_setopt($curl, CURLOPT_POST, 1);
            if (!empty($this->params))
                curl_setopt($curl, CURLOPT_POSTFIELDS, $this->params);
        }

        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36');
        curl_setopt($curl, CURLOPT_COOKIEJAR, "cookie.txt");
        curl_setopt($curl, CURLOPT_COOKIEFILE, "cookie.txt");
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

        $res = curl_exec($curl);

        curl_close($curl);

        return $res;
    }
}