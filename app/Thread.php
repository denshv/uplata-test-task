<?php

namespace app;

use  \Symfony\Component\DomCrawler\Crawler;

/**
 * Thread is a class that used for working with forum thread
 *
 */
class Thread
{

    const USER_NOT_AUTHORIZED = 'Пользователь не авторизован';

    const THREAD_TITLE_NOT_FOUND = '';

    /**
     * Thread id
     *
     * @var null
     */
    protected $id = null;

    /**
     * Thread url
     *
     * @var null
     */
    protected $url = null;

    /**
     * Instance of symfony DomCrawler
     *
     * @var Crawler
     */
    protected $crawler;

    /**
     * Instance of Request
     *
     * @var Request
     */
    protected $request;


    /**
     * Create a new Thread instance.
     *
     * @param string $url - url of thread
     */
    public function __construct($url)
    {
        $this->url = $url;

        if (empty($url))
            die('Укажите url темы');

        $this->request = new Request();
    }

    /**
     * Get Thread posts.
     *
     * @return array
     */
    public function get():array
    {
        $page = $this->request->get($this->url);

        $this->crawler = new Crawler($page);

        $array = $this->crawler->filter('#postlist>#posts>li')->each(function (Crawler $node, $i) {

            $a = [];

            $date = $node->filter('.posthead .postdate .date');

            $author = $node->filter('.postdetails .userinfo .username_container a.username strong');
            $header = $node->filter('.postdetails .postbody .postrow h2.title');
            $text = $node->filter('.postdetails .postbody .postrow>.content');

            if ($date->count() > 0) {
                $a['date'] = $date->text();
            }

            if ($author->count() > 0) {
                $a['author'] = $author->text();
            }

            if ($header->count() > 0) {
                $a['header'] = trim($header->text());
            }

            if ($text->count() > 0) {
                $a['text'] = trim($text->text());
            }

            return !empty($a) ? $a : null;
        });

        $array = array_filter($array, function ($item) {
            return !empty($item) ? $item : false;
        });

        return $array;
    }

    /**
     * Get authentificate username
     *
     * @return string
     */
    public function getCurrentUser():string
    {
        $user = $this->crawler->filter('#toplinks li.welcomelink a');

        if ($user->count() > 0) {
            return trim($user->text());
        }

        return self::USER_NOT_AUTHORIZED;
    }

    /**
     * Get title of Thread
     *
     * @return string
     */
    protected function getTitle():string
    {
        $title = $this->crawler->filter('#pagetitle .threadtitle a');
        if ($title->count() > 0) {
            return trim($title->text());
        }

        return self::THREAD_TITLE_NOT_FOUND;
    }

    /**
     * Save posts to file
     *
     * @param array $posts
     *
     * @return bool
     */
    public function save(array $posts):bool
    {
        foreach ($posts as $post) {

            Storage::log('log/' . $this->getTitle() . '_' . $post['date'], print_r($post, true));
        }

        return true;
    }
}