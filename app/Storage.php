<?php

namespace app;

/**
 * Storage is a class that simple implements File Storage
 */
class Storage
{

    public function __construct()
    {

    }

    /**
     * Function frite string to file
     *
     * @param string $filename
     * @param string $str
     * 
     * @return void
     */
    public static function log(string $filename, string $str):void
    {

        $fp = fopen($filename, 'a+');
        if ($fp) {
            fwrite($fp, $str . "\n");
        }
    }
}