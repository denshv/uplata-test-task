<?php

define('BASE_PATH', __DIR__);
define('SITE_URL', 'http://forumodua.com/');

/*
 * ID Of theme
 */
define('THREAD_ID_DEFAULT', 189283);

define('THREAD_URL', SITE_URL . 'showthread.php?t=');
define('LOGIN_URL', SITE_URL . 'login.php?do=login');

/*
 * Config to auth
 */
define('LOGIN', '');
define('PASS', '');
